﻿using System.Web.Mvc;
using Test007.Models;

namespace Test007.Controllers
{
	public class PlanController : Controller
    {

		private PlanRepository _repo;
		private PlanRepository Repo
		{
			get
			{
				if (_repo == null)
				{
					_repo = new PlanRepository();
				}
			}
		}

		// GET: Factors
		public ActionResult Index()
        {
            return View("Index", _repo.GetAll());
        }

        // GET: Factors/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Factors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Factors/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Factors/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Factors/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Factors/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Factors/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
//https://www.c-sharpcorner.com/article/asp-net-mvc-crud-with-dapper-micro-orm/