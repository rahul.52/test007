﻿using Dapper;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Test007.Models
{
    public class PlanRepository : IPlanRepository<Plan>
    {

        private IDbConnection _db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);



        public List<Plan> GetAll()
        {
            return this._db.Query<Plan>("Select * FROM \"Plan\"").ToList();
        }



        public Plan Find(int PlanId)
        {
            return this._db.Query<Plan>("Select * FROM \"Plan\" Where \"PlanId\" = @PlanId", new { PlanId }).SingleOrDefault();
        }
    }
}
