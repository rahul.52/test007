﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test007.Models
{
    public class Factor
    {
        public int Id { get; set; }
        public string  PlanName { get; set; }
        public float Sa { get; set; }
        public float Ta { get; set; }
        public Plan Plan{ get; set; }
        public int PlanId { get; set; }
    }
}