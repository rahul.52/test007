﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Test007.Models
{
    public interface IPlanRepository<Plan> 
    {
        List<Plan> GetAll();
        Plan Find(int PlanId);
        
    }
}