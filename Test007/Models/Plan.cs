﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test007.Models
{
    public class Plan
    {
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public bool Sa { get; set; }
        public bool Ta { get; set; }
    }
}